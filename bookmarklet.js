 var text = document.body.innerText,
          words = text.split(" "),
          filteredWords = words.map(getValidWords).filter(Boolean);

      function getValidWords (item, index) {
        if(item.length >= 5) {
          return item ;
        }
      }

      function showDuplicates(og) {
        var compressed = [];
        var copy = og.slice(0);

        for(var i = 0; i < og.length; i++) {

          var myCount = 0;

          for(var w = 0; w < copy.length; w++) {
            if(og[i] == copy[w]){
              myCount++;
              delete copy[w];
            }
          }

          if(myCount > 0) {
            var a = new Object;
            a.value = og[i];
            a.count = myCount;
            if(a.count <= 5) {
            a.boldness = "sm";
            } else if (a.count >= 6 && a.count <= 9) {
            a.boldness = "md";
            }else {
            a.boldness = "lg";
            }

            compressed.push(a);
          }

        }
        return compressed;
      }
      
    var wordList = showDuplicates(filteredWords);
    var markup =  `
                    <style>
                    ul {
                      border: 3px solid black;
                    }
                    .word-list li {
                      list-style-type: none;
                      padding: 20px;
                    }
                    .word-list li.sm {
                      font-size: 14px;
                      font-weight: 200;
                    }
                    .word-list li.md {
                      font-size: 18px;
                      font-weight: 400;
                    }
                    .word-list li.lg {
                      font-size: 24px;
                      font-weight: 700;
                    }
                    </style>
                    <ul class="word-list">${wordList.map(word => `<li class='${word.boldness}'>${word.value}</li>`).join('')}</ul>`;
                  document.body.innerHTML = markup;